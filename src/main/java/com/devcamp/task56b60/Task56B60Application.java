package com.devcamp.task56b60;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56B60Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56B60Application.class, args);
	}

}
