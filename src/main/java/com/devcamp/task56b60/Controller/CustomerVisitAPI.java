package com.devcamp.task56b60.Controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b60.Customer;
import com.devcamp.task56b60.Visit;

@RestController
@RequestMapping("/api")
public class CustomerVisitAPI {
    @CrossOrigin
    @GetMapping("/visits")

    public ArrayList<Visit> getVisits(){
        Customer customer1 = new Customer("Nguyễn Việt Phương");
        Customer customer2 = new Customer("Lê Thị Hiền");
        Customer customer3 = new Customer("Đặng Ngọc Viễn");

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        Visit visit3 = new Visit(customer3, new Date());
       
        visit1.setProductExpense(2.0);
        visit1.setDate(new Date(12/12/2022));
        System.out.println(visit1);
        System.out.println(visit2);
        System.out.println(visit3);

        ArrayList<Visit> arrListVisit = new ArrayList<Visit>();

        arrListVisit.add(visit1);
        arrListVisit.add(visit2);
        arrListVisit.add(visit3);

        return arrListVisit;
    }
}
